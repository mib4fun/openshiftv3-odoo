FROM odoo:8

COPY ./entrypoint.sh /
COPY ./openerp-server.conf /etc/odoo

COPY ./addons /mnt/extra-addons
COPY ./native-addons /usr/lib/python2.7/dist-packages/openerp/addons

USER root

RUN mkdir /var/lib/odoo/sessions && chown -R odoo:odoo /var/lib/odoo/sessions

USER odoo

ENTRYPOINT ["/entrypoint.sh"]
CMD ["openerp-server"]
