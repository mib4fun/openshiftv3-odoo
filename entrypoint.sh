#!/bin/bash

set -e
export

# set odoo database host, port, user and password
# extract the protocol
parse_url() {
    eval $(echo "$1" | sed -e "s#^\(\(.*\)://\)\?\(\([^:@]*\)\(:\(.*\)\)\?@\)\?\([^/?]*\)\(/\(.*\)\)\?#${PREFIX:-DB_}SCHEME='\2' ${PREFIX:-DB_}USER='\4' ${PREFIX:-DB_}PASSWORD='\6' ${PREFIX:-DB_}HOST='\7' ${PREFIX:-DB_}PATH='\9'#")
  }

PREFIX="DB_" parse_url "$DATABASE_URL"
echo USER $DB_USER
echo HOST $DB_HOST
DB_USER=odoo
DB_PORT=5432
DB_PASS=odoo
DB_PASSWORD=$DB_PASS
DB_HOST=172.30.139.168
DB_PATH=isante
echo USER $DB_USER
echo PORT $DB_PORT
echo PASS $DB_PASS
echo HOST $DB_HOST
echo PATH $DB_PATH

options="--db_host=$DB_HOST --db_port=$DB_PORT --db_user=$DB_USER --db_password=$DB_PASSWORD --database=$DB_PATH"
echo $options

openerp-server --help
openerp-server $options

exit 1
